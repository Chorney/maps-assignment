# Capstone Demo Application for Ruby on Rails Specialization
Final assignment for the ruby on rails specialation. Last assignment focus is on integration with google maps

# Commands to get project up and running

* docker-compose run railsvm rake db:drop
* docker-compose run railsvm rake db:create
* docker-compose run railsvm rake db:migrate
* docker-compose run railsvm rake db:mongoid:create_indexes
* docker-compose run railsvm rake ptourist:reset_all RAILS_ENV=development
* docker-compose up railsvm

# Dependencies
* google-cloud Directions API
* Geolocation API
* Maps Javascript API
* Postgres
* MongoDB

# Note: 
* must update the api key for google cloud services to include directions services, removed it so account is not being queried

# Assignment Directions:
* [Navigate to the website](https://drchorne-capstone-staging.herokuapp.com/)
* Select downtown baltimore as the origin
* go to trips from the drop down menu
* change the miles > than 10
* should see all the different routes show up, and the ui should update depending on the route distance
